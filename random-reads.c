/* vim: set sts=4 sw=4 et :
 *
 * Copyright (c) 2012, Collabora Ltd.
 *
 * Author: Nirbheek Chauhan <nirbheek.chauhan@collabora.co.uk>
 * License: LGPL v2.1
 *
 * Do tons of random reads on the given file
 *
 */

#define _GNU_SOURCE
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <time.h>

#define handle_error(msg) \
  do { perror(msg); exit(EXIT_FAILURE); } while (0)

int main (int	argc,
	  char *argv[])
{
    char *filename = NULL;
    struct stat st;
    ssize_t file_size;
    int fd, ret;

    if (argc < 2) {
        printf ("Usage: %s <file to read>\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    filename = argv[1];

    ret = stat(filename, &st);
    if (ret < 0)
        handle_error ("stat");

    if (!S_ISREG(st.st_mode)) {
        printf ("'%s' is not a regular file!\n", filename);
        exit(EXIT_FAILURE);
    }

    file_size = st.st_size;
    fd = open (filename, O_RDONLY);
    if (fd == -1)
        handle_error ("open");

    /* Chunk size to read; in bytes. Needs to be tuned with hdd perf. */
    ssize_t chunk_size = 1024*1024;
    off_t random_offset;
    char buf[chunk_size];
    /* Scale this with the IOPS rating of the drive */
    int n = 10000;
    while (n--) {
        /* Seek to a random location in the file */
        srandom(time(NULL)+n);
        random_offset = random() % file_size;
        lseek (fd, random_offset, SEEK_SET);
        /* Read some data at that offset */
        ret = read (fd, &buf, chunk_size);
        if (ret == -1)
            handle_error ("read");
#ifdef DEBUG
        printf ("Read %d bytes at offset %d\n", ret, random_offset);
#endif
    }
}
